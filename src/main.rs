use std::io;
use tui::{
    Terminal,
    backend::CrosstermBackend,
    widgets::Paragraph,
    layout::{Layout, Constraint, Direction},
    text::{Spans, Span},
    style::{Style, Color}
};
use crossterm::{
    event::{DisableMouseCapture, EnableMouseCapture, Event, KeyCode, self},
    execute,
    terminal::{
        disable_raw_mode, enable_raw_mode,
        EnterAlternateScreen, LeaveAlternateScreen
    },
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut terminal = setup_terminal()?;

    let mut should_exit = false;

    loop {
        terminal.draw(|rect| {
            let size = rect.size();
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .constraints([Constraint::Percentage(100)].as_ref())
                .split(size);

            let text = Spans::from(vec![
                Span::styled("Hello, world!", Style::default().fg(Color::Yellow)),
            ]);

            let paragraph = Paragraph::new(text)
                .style(Style::default().fg(Color::Yellow))
                .wrap(tui::widgets::Wrap { trim: true });

            rect.render_widget(paragraph, chunks[0]);
        })?;

        // Check for an exit condition (e.g., pressing a specific key)
        // and break the loop if necessary
        // ...

        // Add any necessary delay or input handling
        if should_exit {
            break;
        }

        if let Event::Key(key_event) = event::read()? {
            if key_event.code == KeyCode::Char('q') {
                should_exit = true;
            }
        }


        // Clear the terminal before the next frame
        // thread::sleep(Duration::from_millis(200));
        // terminal.clear()?;
    }



    restore_terminal(terminal)?;

    Ok(())
}

fn setup_terminal() -> Result<Terminal<CrosstermBackend<io::Stdout>>, Box<dyn std::error::Error>> {
    enable_raw_mode()?;
    let mut stdout = io::stdout();

    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;

    let backend = CrosstermBackend::new(stdout);
    let terminal = Terminal::new(backend)?;

    Ok(terminal)
}

fn restore_terminal(mut terminal: Terminal<CrosstermBackend<io::Stdout>>) -> Result<(), Box<dyn std::error::Error>> {
    // Restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;
    
    Ok(())
}